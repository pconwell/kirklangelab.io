---
published:  true
layout:     page
title:      About Me
date:       2018-05-21 22:00:00
author:     Kirk Lange
summary:    Who am I? What do I do? Look no further!
thumbnail:  file-alt
---

<h1 id="toc">
<center>
<div style="padding-bottom:10px">Table of Contents</div>
<a href="#-education-" style="color:white;"><i class="fa fa-graduation-cap"></i></a> &nbsp;
<a href="#-skills-" style="color:white;"><i class="fa fa-wrench"></i></a> &nbsp;
<a href="#-projects-" style="color:white;"><i class="fa fa-file-code"></i></a> &nbsp;
<a href="#-work-experience-" style="color:white;"><i class="fa fa-briefcase"></i></a> &nbsp;
<a href="#-languages-" style="color:white;"><i class="fa fa-language"></i></a>
</center>
</h1>


<br>
# <i class="fa fa-graduation-cap"></i> Education <span style="float:right;"><a href="#" style="color:white;"><i class="fas fa-arrow-circle-up"></i></a></span>
***
### **B.A. in Computer Science** (Pursuing)
#### *Aug 2016 - May 2019* <small>(3rd Year Graduation)</small>
#### *Whitman College*
#### *Walla Walla, WA*
- Current GPA
  - Major: 3.90
  - Cumulative: 3.91
- Coursework
  - Data Structures (C++)
  - Systems Programming (C, Assembly)
  - Artificial Intelligence (<a target="_blank" href="https://gitlab.com/kirklange/royal-game-of-ur">final project</a>)
  - Natural Language Processing (Python)
  - Software Engineering (<a target="_blank" href="https://github.com/whitman-books-online/whitman-books-online">class project</a>)
  - Discrete Math and Functional Programming (SML)
  - Algorithm Design and Analysis
  - Theory of Computation
  - Computer Architecture
- Leadership
  - Founder/President of STEM4ALL residence hall suite (2017 - 2018)
  - Vice President of Martial Arts club (2017)

<br>
# <i class="fa fa-wrench"></i> Skills <span style="float:right;"><a href="#" style="color:white;"><i class="fas fa-arrow-circle-up"></i></a></span>
****
### **Programming and Scripting**
- C, C++, Python, Java
- SDL2 graphics library
- Make, Shell, CMake/CTest
- Writing portable, cross-platform code
- Efficiently working with large datasets

### **Software Development**
- Git and collaborative software development
- Auto documentation with Doxygen and Sphinx
- Continuous integration (Travis CI, AppVeyor)
- Code coverage (Codecov, Coveralls)

### **Miscellaneous**
- Completing tasks under tight deadlines
- Self-teaching and on-the-job learning
- Presentations and public speaking

<br>
# <i class="fa fa-file-code"></i> Projects <span style="float:right;"><a href="#" style="color:white;"><i class="fas fa-arrow-circle-up"></i></a></span>
****
### **<a target="_blank" href="https://github.com/kirklange/accompaniment-pairer">Accompaniment Pairer</a>** (C++)
#### *Aug 2017*
- Paired four dozen musicians with piano accompanists based on schedule compatibility
- Eliminated need to spend hours manually sorting through student schedules
- Implemented tree traversal algorithm to find optimal set of pairings

### **<a target="_blank" href="https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/1284860-1-5-2-crazy-ravines-and-caves-mod">Crazy Ravines Minecraft Mod</a>** (Java)
#### *Aug 2012*
- 10,000 downloads over its one-year lifespan
- Featured on dozens of YouTube channels and international forums
- Maintained consistent audience by addressing community feedback
  - 7500 downloads in the first six months
  - 2500 downloads in the following six months

<br>
# <i class="fa fa-briefcase"></i> Work Experience <span style="float:right;"><a href="#" style="color:white;"><i class="fas fa-arrow-circle-up"></i></a></span>
****
### **Research Assistant**
#### *May 2017 - Jul 2017*
#### *Whitman College*
#### *Walla Walla, WA*
- C++ software development of <a target="_blank" href="https://github.com/johnastratton/DelayDifferentialEqnSimulator">gene regulation network simulation</a>
- Optimized simulation runtime by 20%
- Implemented dataset API for files with +100k time-steps
- Automated simulation per-model recompilation with CMake
- Refactored codebase with emphasis on modularity and configurability
- Implemented file and command-line I/O classes for logging and configuration
- Monthly, formal presentations to the CS department on progress and results
- Awarded 3rd place student poster presentation at CCSC-NW 2017 Conference

### **Museum Guide / Camp Councelor**
#### *Jul 2015*
#### *The Science Factory*
#### *Eugene, OR*
- Engaged visitors of all ages in museum exhibits and activities
- Supervised experiments and activities in chemsitry and astronomy for two dozen campers

<br>
# <i class="fa fa-language"></i> Languages <span style="float:right;"><a href="#" style="color:white;"><i class="fas fa-arrow-circle-up"></i></a></span>
****
### **English**
- Native language

### **French**
- Professional proficiency
- AP French: 5/5
- IB French B HL: 5/7

### **Chinese**
- Basic proficiency
